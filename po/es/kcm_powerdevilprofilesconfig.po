# Spanish translations for powerdevilprofilesconfig.po package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2014.
# SPDX-FileCopyrightText: 2014, 2020, 2023, 2024 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: powerdevilprofilesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-18 00:41+0000\n"
"PO-Revision-Date: 2024-05-25 01:39+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.2\n"

#: PowerKCM.cpp:455
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Parece que el servicio de gestión de energía no está funcionando."

#: ui/DurationPromptDialog.qml:130
#, kde-format
msgctxt "The unit of the time input field"
msgid "millisecond"
msgid_plural "milliseconds"
msgstr[0] "milisegundo"
msgstr[1] "milisegundos"

#: ui/DurationPromptDialog.qml:132
#, kde-format
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] "segundo"
msgstr[1] "segundos"

#: ui/DurationPromptDialog.qml:134
#, kde-format
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minuto"
msgstr[1] "minutos"

#: ui/DurationPromptDialog.qml:136
#, kde-format
msgctxt "The unit of the time input field"
msgid "hour"
msgid_plural "hours"
msgstr[0] "hora"
msgstr[1] "horas"

#: ui/DurationPromptDialog.qml:138
#, kde-format
msgctxt "The unit of the time input field"
msgid "day"
msgid_plural "days"
msgstr[0] "día"
msgstr[1] "días"

#: ui/DurationPromptDialog.qml:140
#, kde-format
msgctxt "The unit of the time input field"
msgid "week"
msgid_plural "weeks"
msgstr[0] "semana"
msgstr[1] "semanas"

#: ui/DurationPromptDialog.qml:142
#, kde-format
msgctxt "The unit of the time input field"
msgid "month"
msgid_plural "months"
msgstr[0] "mes"
msgstr[1] "meses"

#: ui/DurationPromptDialog.qml:144
#, kde-format
msgctxt "The unit of the time input field"
msgid "year"
msgid_plural "years"
msgstr[0] "año"
msgstr[1] "años"

#: ui/DurationPromptDialog.qml:171
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "milliseconds"
msgstr "milisegundos"

#: ui/DurationPromptDialog.qml:173
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "seconds"
msgstr "segundos"

#: ui/DurationPromptDialog.qml:175
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "minutes"
msgstr "minutos"

#: ui/DurationPromptDialog.qml:177
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "hours"
msgstr "horas"

#: ui/DurationPromptDialog.qml:179
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "days"
msgstr "días"

#: ui/DurationPromptDialog.qml:181
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "weeks"
msgstr "semanas"

#: ui/DurationPromptDialog.qml:183
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "months"
msgstr "meses"

#: ui/DurationPromptDialog.qml:185
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "years"
msgstr "años"

#: ui/GlobalConfig.qml:16
#, kde-format
msgctxt "@title"
msgid "Advanced Power Settings"
msgstr "Preferencias avanzadas de energía"

#: ui/GlobalConfig.qml:21
#, kde-format
msgctxt ""
"Percentage value example, used for formatting battery levels in the power "
"management settings"
msgid "10%"
msgstr "10 %"

#: ui/GlobalConfig.qml:57
#, kde-format
msgctxt "@title:group"
msgid "Battery Levels"
msgstr "Niveles de batería"

#: ui/GlobalConfig.qml:65
#, kde-format
msgctxt ""
"@label:spinbox Low battery level percentage for the main power supply battery"
msgid "&Low level:"
msgstr "Nive&l bajo:"

#: ui/GlobalConfig.qml:73
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level"
msgstr "Nivel de batería bajo"

#: ui/GlobalConfig.qml:98
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered low when it drops to this level. "
"Settings for low battery will be used instead of regular battery settings."
msgstr ""
"La carga de la batería se considerará baja cuando esté por debajo de este "
"nivel. Se usarán las preferencias de batería baja en lugar de las de batería "
"normal."

#: ui/GlobalConfig.qml:105
#, kde-format
msgctxt ""
"@label:spinbox Critical battery level percentage for the main power supply "
"battery"
msgid "Cr&itical level:"
msgstr "N&ivel crítico:"

#: ui/GlobalConfig.qml:113
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Critical battery level"
msgstr "Nivel de batería crítico"

#: ui/GlobalConfig.qml:138
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered critical when it drops to this level. "
"After a brief warning, the system will automatically suspend or shut down, "
"according to the configured critical battery level action."
msgstr ""
"La carga de la batería se considerará crítica cuando esté por debajo de este "
"nivel. Tras una breve advertencia, el sistema se suspenderá o se apagará "
"automáticamente, según la acción que haya configurado para el nivel de "
"batería crítico."

#: ui/GlobalConfig.qml:147
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for reaching the critical battery "
"level is now unsupported on your system. Please select a different one."
msgstr ""
"La acción que había configurado anteriormente para cuando se alcanza el "
"nivel crítico de batería ya no está permitida en su sistema. Seleccione otra."

#: ui/GlobalConfig.qml:154
#, kde-format
msgctxt ""
"@label:combobox Power action such as sleep/hibernate that will be executed "
"when the critical battery level is reached"
msgid "A&t critical level:"
msgstr "En el nivel crí&tico:"

#: ui/GlobalConfig.qml:156
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action performed at critical battery level"
msgstr "Acción realizada en el nivel de batería crítico"

#: ui/GlobalConfig.qml:181
#, kde-format
msgctxt "@label:spinbox Low battery level percentage for peripheral devices"
msgid "Low level for peripheral d&evices:"
msgstr "Niv&el bajo para dispositivos periféricos:"

#: ui/GlobalConfig.qml:189
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level for peripheral devices"
msgstr "Nivel de batería bajo para dispositivos periféricos"

#: ui/GlobalConfig.qml:214
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge for peripheral devices will be considered low when it "
"reaches this level."
msgstr ""
"La carga de la batería de los dispositivo periféricos se considerará baja "
"cuando alcance este nivel."

#: ui/GlobalConfig.qml:222
#, kde-format
msgctxt "@title:group"
msgid "Charge Limit"
msgstr "Límite de carga"

#: ui/GlobalConfig.qml:233
#, kde-format
msgctxt "@label:checkbox"
msgid "&Battery protection:"
msgstr "Protección de la &batería:"

#: ui/GlobalConfig.qml:234
#, kde-format
msgctxt "@text:checkbox"
msgid "Limit the maximum battery charge"
msgstr "Limitar la carga máxima de la batería"

#: ui/GlobalConfig.qml:242
#, kde-format
msgctxt ""
"@label:spinbox Battery will stop charging when this charge percentage is "
"reached"
msgid "&Stop charging at:"
msgstr "De&jar de cargar al:"

#: ui/GlobalConfig.qml:268
#, kde-format
msgctxt ""
"@label:spinbox Battery will start charging again when this charge percentage "
"is reached, after having hit the stop-charging threshold earlier"
msgid "Start charging once &below:"
msgstr "Empezar a cargar por de&bajo de:"

#: ui/GlobalConfig.qml:325
#, kde-format
msgctxt "@info:status"
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"Tendrá que desconectar y volver a conectar la fuente de alimentación para "
"empezar a cargar la batería de nuevo."

#: ui/GlobalConfig.qml:336
#, kde-format
msgctxt "@info"
msgid ""
"Regularly charging the battery close to 100%, or fully discharging it, may "
"accelerate deterioration of battery health. By limiting the maximum battery "
"charge, you can help extend the battery lifespan."
msgstr ""
"La carga habitual de la batería cerca del 100 %, o su carga completa, puede "
"acelerar el deterioro del estado de salud de la batería. Si limita la carga "
"máxima de la batería, podrá extender su vida."

#: ui/GlobalConfig.qml:344
#, kde-format
msgctxt ""
"@title:group Miscellaneous power management settings that didn't fit "
"elsewhere"
msgid "Other Settings"
msgstr "Otras preferencias"

#: ui/GlobalConfig.qml:352
#, kde-format
msgctxt "@label:checkbox"
msgid "&Media playback:"
msgstr "Reproducción &multimedia:"

#: ui/GlobalConfig.qml:353
#, kde-format
msgctxt "@text:checkbox"
msgid "Pause media players when suspending"
msgstr "Pausar los reproductores multimedia al entrar en suspensión"

#: ui/GlobalConfig.qml:366
#, kde-format
msgctxt "@label:button"
msgid "Related pages:"
msgstr "Páginas relacionadas:"

#: ui/GlobalConfig.qml:381
#, kde-format
msgctxt "@text:button Name of KCM, plus Power Management notification category"
msgid "Notifications: Power Management"
msgstr "Notificaciones: Gestión de energía"

#: ui/GlobalConfig.qml:383
#, kde-format
msgid "Open \"Notifications\" settings page, \"Power Management\" section"
msgstr ""
"Abrir la página de preferencias de «Notificaciones» en la sección «Gestión "
"de energía»"

#: ui/GlobalConfig.qml:390
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Desktop Session"
msgstr "Sesión de escritorio"

#: ui/GlobalConfig.qml:391
#, kde-format
msgid "Open \"Desktop Session\" settings page"
msgstr "Abrir la página de preferencias «Sesión de escritorio»"

#: ui/GlobalConfig.qml:398
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Activities"
msgstr "Actividades"

#: ui/GlobalConfig.qml:399
#, kde-format
msgid "Open \"Activities\" settings page"
msgstr "Abrir la página de preferencias de las «Actividades»"

#: ui/main.qml:19
#, kde-format
msgctxt "@action:button"
msgid "Advanced Power &Settings…"
msgstr "Preferencias a&vanzadas de energía..."

#: ui/main.qml:43
#, kde-format
msgctxt "@text:placeholdermessage"
msgid "Power Management settings could not be loaded"
msgstr "No se pueden cargar las preferencias de gestión de energía"

#: ui/main.qml:65
#, kde-format
msgid "On AC Power"
msgstr "Con energía AC"

#: ui/main.qml:66
#, kde-format
msgid "On Battery"
msgstr "Con batería"

#: ui/main.qml:67
#, kde-format
msgid "On Low Battery"
msgstr "Con batería baja"

#: ui/ProfileConfig.qml:24
#, kde-format
msgctxt ""
"Percentage value example, used for formatting brightness levels in the power "
"management settings"
msgid "10%"
msgstr "10%"

#: ui/ProfileConfig.qml:33
#, kde-format
msgctxt "@title:group"
msgid "Suspend Session"
msgstr "Suspender la sesión"

#: ui/ProfileConfig.qml:48
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for after a period of inactivity is "
"now unsupported on your system. Please select a different one."
msgstr ""
"La acción que había configurado anteriormente para cuando se alcanza un "
"período de inactividad ya no está permitida en su sistema. Seleccione otra."

#: ui/ProfileConfig.qml:55
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"system is idle"
msgid "A&fter a period of inactivity:"
msgstr "Tras un período de inacti&vidad:"

#: ui/ProfileConfig.qml:63
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the system is idle"
msgstr "Acción que se realizará cuando el sistema esté inactivo"

#: ui/ProfileConfig.qml:95
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 second"
msgid_plural "after %1 seconds"
msgstr[0] "tras %1 segundo"
msgstr[1] "tras %1 segundos"

#: ui/ProfileConfig.qml:97
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 minute"
msgid_plural "after %1 minutes"
msgstr[0] "tras %1 minuto"
msgstr[1] "tras %1 minutos"

#: ui/ProfileConfig.qml:110 ui/ProfileConfig.qml:383 ui/ProfileConfig.qml:526
#: ui/ProfileConfig.qml:862
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values"
msgid "Custom…"
msgstr "Personalizado..."

#: ui/ProfileConfig.qml:148
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the power button is "
"pressed is now unsupported on your system. Please select a different one."
msgstr ""
"La acción que había configurado anteriormente para cuando se pulsa el botón "
"de encendido ya no está permitida en su sistema. Seleccione otra."

#: ui/ProfileConfig.qml:155
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When &power button pressed:"
msgstr "Cuando se &pulse el botón de encendido:"

#: ui/ProfileConfig.qml:157
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the power button is pressed"
msgstr "Acción que se realizará cuando se pulse el botón de encendido"

#: ui/ProfileConfig.qml:187
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the lid is closed is now "
"unsupported on your system. Please select a different one."
msgstr ""
"La acción que había configurado anteriormente para cuando se cierra la tapa "
"ya no está permitida en su sistema. Seleccione otra."

#: ui/ProfileConfig.qml:194
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When laptop &lid closed:"
msgstr "Cuando se cierre &la tapa del portátil:"

#: ui/ProfileConfig.qml:196
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the laptop lid is closed"
msgstr "Acción que se realizará cuando se cierre la tapa del portátil"

#: ui/ProfileConfig.qml:225
#, kde-format
msgctxt ""
"@text:checkbox Trigger laptop lid action even when an external monitor is "
"connected"
msgid "Even when an external monitor is connected"
msgstr "Incluso cuando haya un monitor externo conectado"

#: ui/ProfileConfig.qml:227
#, kde-format
msgid "Perform laptop lid action even when an external monitor is connected"
msgstr ""
"Realizar la acción del cierre de la tapa incluso cuando haya un monitor "
"externo conectado"

#: ui/ProfileConfig.qml:245
#, kde-format
msgctxt "@info:status"
msgid ""
"The sleep mode you had previously configured is now unsupported on your "
"system. Please select a different one."
msgstr ""
"El modo de reposo que había configurado anteriormente ya no está permitido "
"en su sistema. Seleccione otro."

#: ui/ProfileConfig.qml:252
#, kde-format
msgctxt ""
"@label:combobox Sleep mode selection - suspend to memory, disk or both"
msgid "When sleeping, enter:"
msgstr "En estado de reposo, entrar:"

#: ui/ProfileConfig.qml:254
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "When sleeping, enter this power-save mode"
msgstr "En estado de reposo, entrar en este modo de ahorro de energía"

#: ui/ProfileConfig.qml:303
#, kde-format
msgctxt "@title:group"
msgid "Display and Brightness"
msgstr "Pantalla y brillo"

#: ui/ProfileConfig.qml:313
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change scr&een brightness:"
msgstr "Ca&mbiar el brillo de la pantalla:"

#: ui/ProfileConfig.qml:359
#, kde-format
msgctxt "@label:combobox Dim screen after X minutes"
msgid "Di&m automatically:"
msgstr "At&enuar automáticamente:"

#: ui/ProfileConfig.qml:362
#, kde-format
msgctxt "@label:spinbox Dim screen after X minutes"
msgid "Di&m automatically after:"
msgstr "At&enuar automáticamente tras:"

#: ui/ProfileConfig.qml:367
#, kde-format
msgctxt "@option:combobox"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segundo"
msgstr[1] "%1 segundos"

#: ui/ProfileConfig.qml:369
#, kde-format
msgctxt "@option:combobox"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: ui/ProfileConfig.qml:376
#, kde-format
msgctxt "@option:combobox Dim screen automatically"
msgid "Never"
msgstr "Nunca"

#: ui/ProfileConfig.qml:428
#, kde-format
msgctxt "@info:status"
msgid ""
"The screen will not be dimmed because it is configured to turn off sooner."
msgstr ""
"La pantalla no se atenuará porque está configurada para apagarse antes."

#: ui/ProfileConfig.qml:433
#, kde-format
msgctxt "@label:combobox After X minutes"
msgid "&Turn off screen:"
msgstr "Apagar la pan&talla:"

#: ui/ProfileConfig.qml:446
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "Turn off screen after:"
msgstr "Apagar la pantalla tras:"

#: ui/ProfileConfig.qml:450
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segundo"
msgstr[1] "%1 segundos"

#: ui/ProfileConfig.qml:452
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: ui/ProfileConfig.qml:459
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "Never"
msgstr "Nunca"

#: ui/ProfileConfig.qml:466
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values "
"(caution: watch for string length)"
msgid "Custom…"
msgstr "Personalizado..."

#: ui/ProfileConfig.qml:506
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "When locked, turn off screen after:"
msgstr "Cuando esté bloqueada, apagar la pantalla tras:"

#: ui/ProfileConfig.qml:510
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 second"
msgid_plural "When locked: %1 seconds"
msgstr[0] "Cuando esté bloqueada: %1 segundo"
msgstr[1] "Cuando esté bloqueada: %1 segundos"

#: ui/ProfileConfig.qml:512
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 minute"
msgid_plural "When locked: %1 minutes"
msgstr[0] "Cuando esté bloqueada: %1 minuto"
msgstr[1] "Cuando esté bloqueada: %1 minutos"

#: ui/ProfileConfig.qml:519
#, kde-format
msgctxt ""
"@option:combobox Turn off screen after X minutes regardless of lock screen "
"(caution: watch for string length)"
msgid "When locked and unlocked"
msgstr "Cuando esté bloqueada y desbloqueada"

#: ui/ProfileConfig.qml:521
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: Immediately"
msgstr "Cuando esté bloqueada: inmediatamente"

#: ui/ProfileConfig.qml:560
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change key&board brightness:"
msgstr "Cambiar el b&rillo del teclado:"

#: ui/ProfileConfig.qml:607
#, kde-format
msgctxt "@title:group"
msgid "Other Settings"
msgstr "Otras preferencias"

#: ui/ProfileConfig.qml:615
#, kde-format
msgctxt ""
"@label:combobox Power Save, Balanced or Performance profile - same options "
"as in the Battery applet"
msgid "Switch to po&wer profile:"
msgstr "Cambiar al per&fil de energía:"

#: ui/ProfileConfig.qml:619
#, kde-format
msgctxt ""
"@accessible:name:combobox Power Save, Balanced or Performance profile - same "
"options as in the Battery applet"
msgid "Switch to power profile"
msgstr "Cambiar al perfil de energía"

#: ui/ProfileConfig.qml:646
#, kde-format
msgctxt "@label:button"
msgid "Run custom script:"
msgstr "Ejecutar guion personalizado:"

#: ui/ProfileConfig.qml:656
#, kde-format
msgctxt ""
"@text:button Determine what will trigger a script command to run in this "
"power state"
msgid "Choose run conditions…"
msgstr "Escoger las condiciones de ejecución..."

#: ui/ProfileConfig.qml:658
#, kde-format
msgctxt "@accessible:name:button"
msgid "Choose run conditions for script command"
msgstr "Escoger las condiciones de ejecución para el guion"

#: ui/ProfileConfig.qml:677
#, kde-format
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When entering \"%1\" state"
msgstr "Al entrar en el estado «%1»"

#: ui/ProfileConfig.qml:695
#, kde-format
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When exiting \"%1\" state"
msgstr "Al salir del estado «%1»"

#: ui/ProfileConfig.qml:713
#, kde-format
msgctxt "@text:action:menu Script command to execute"
msgid "After a period of inactivity"
msgstr "Tras un período de inactividad"

#: ui/ProfileConfig.qml:736
#, kde-format
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&ntering \"%1\" state:"
msgstr "Al e&ntrar en el estado «%1»:"

#: ui/ProfileConfig.qml:740
#, kde-format
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when entering \"%1\" state"
msgstr "Guion que se ejecutará al entrar en el estado «%1»"

#: ui/ProfileConfig.qml:771
#, kde-format
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&xiting \"%1\" state:"
msgstr "Al &salir del estado «%1»:"

#: ui/ProfileConfig.qml:775
#, kde-format
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when exiting \"%1\" state"
msgstr "Guion que se ejecutará al salir del estado «%1»"

#: ui/ProfileConfig.qml:806
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "After a period of inacti&vity:"
msgstr "Tras un período de inacti&vidad:"

#: ui/ProfileConfig.qml:808
#, kde-format
msgctxt "@@accessible:name:textfield"
msgid "Script command after a period of inactivity"
msgstr "Guion que se ejecutará tras un período de inactividad"

#: ui/ProfileConfig.qml:837
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Period of inactivity until the script command executes"
msgstr "Período de inactividad hasta que se ejecuta el guion"

#: ui/ProfileConfig.qml:842
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "Execute script after:"
msgstr "Ejecutar guion tras:"

#: ui/ProfileConfig.qml:847
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 seconds"
msgid_plural "after %1 seconds"
msgstr[0] "tras %1 segundo"
msgstr[1] "tras %1 segundos"

#: ui/ProfileConfig.qml:849
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 minutes"
msgid_plural "after %1 minutes"
msgstr[0] "tras %1 minuto"
msgstr[1] "tras %1 minutos"

#: ui/RunScriptEdit.qml:33
#, kde-format
msgid "Enter command or select file…"
msgstr "Introduzca una orden o seleccione un archivo..."

#: ui/RunScriptEdit.qml:51
#, kde-format
msgid "Select executable file…"
msgstr "Seleccione un archivo ejecutable..."

#: ui/RunScriptEdit.qml:66
#, kde-format
msgid "Select executable file"
msgstr "Seleccionar un archivo ejecutable"

#: ui/TimeDurationComboBox.qml:22
#, kde-format
msgctxt "@title:window"
msgid "Custom Duration"
msgstr "Duración personalizada"

#~ msgctxt ""
#~ "List of recognized strings for 'minutes' in a time delay expression such "
#~ "as 'after 10 min'"
#~ msgid "m|min|mins|minutes"
#~ msgstr "m|min|mins|minutos"

#~ msgctxt ""
#~ "List of recognized strings for 'seconds' in a time delay expression such "
#~ "as 'after 10 sec'"
#~ msgid "s|sec|secs|seconds"
#~ msgstr "s|seg|segs|segundos"

#~ msgctxt ""
#~ "Validator/extractor regular expression for a time delay number and unit, "
#~ "from e.g. 'after 10 min'. Uses recognized strings for minutes and seconds "
#~ "as %1 and %2."
#~ msgid "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"
#~ msgstr "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Eloy Cuadra"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ecuadra@eloihr.net"

#~ msgid ""
#~ "The KDE Power Management System will now generate a set of defaults based "
#~ "on your computer's capabilities. This will also erase all existing "
#~ "modifications you made. Are you sure you want to continue?"
#~ msgstr ""
#~ "El sistema de gestión de energía de KDE generará ahora un conjunto de "
#~ "preferencias por omisión basándose en las capacidades de su equipo. Esto "
#~ "también borrará todas las modificaciones que haya realizado. ¿Seguro que "
#~ "desea continuar?"

#~ msgid "Restore Default Profiles"
#~ msgstr "Restaurar los perfiles por omisión"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Parece que el servicio de gestión de energía no está en ejecución.\n"
#~ "Esto se puede solucionar iniciándolo o programándolo en «Inicio y apagado»"
